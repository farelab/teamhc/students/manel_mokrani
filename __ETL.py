import pandas as pd
import numpy as np
import os 
import re
from sqlalchemy import create_engine
import os.path
from pathlib import Path


#NOTA BENE : Il existe une séquence prédfinie sur pgSql pour les ids séquentiels, no need to récup le max et insérer il s'insére tous seul, enfin i think
# a voir avec les dataframes how does it work, plus tard of course  

#Global var, 1 : ajout , 0 : premier remplissage 
mode = 0 

# df / DF  refers to dataframe, a data structure in python 
# DB/db refers to Database

#Retrieve the path of this python file
BASE_DIR = Path(__file__).resolve().parent

# Connection to pgsql database function
def DB_Connect(uid, password, server, database, port) :
    engine = create_engine(f'postgresql://{uid}:{password}@{server}:{port}/{database}')
    return engine

# Retrieve csv file's path from database 
def File_Path (engine,BASE_DIR,filename,file):
    
    select_Path = "select csf_path from imp.t_csvfile_csf where csf_name = '{0}'".format(filename)
    with engine.connect() as con:
        path = con.execute(select_Path)
    path =  path.fetchall()

    dictPath = path[0]._asdict()
    path = dictPath['csf_path']
    path = path[1:]     # Delete the dash at the begining of the path

    path = Path(path)
    path = BASE_DIR.joinpath(path)
    if file :
        path = path.joinpath(filename + '.csv')
    return path

# load csv files into dataframes
def file_to_df (path):

    df = pd.read_csv(path, delimiter=';', header=0, encoding = "utf-8") 
    return df

# load database tables into dataframes
def dbTbl_to_df (engine, schema, tbl_name):
    
    tbl = ".".join([schema,tbl_name])
    select_tbl = 'SELECT * FROM {0}'.format(tbl)
    tbl = engine.execute(select_tbl)
    df_Tbl = pd.DataFrame(tbl.fetchall())
    return df_Tbl

# Non foreign key tables (NFT) _ Transform data function 
def NFT_Transorm (df_fich, Tcolumns, DfColumns, idT,  sdf_Tbl = pd.DataFrame()):

    To_StoreDf = df_fich[DfColumns]
    To_StoreDf.columns = Tcolumns

    To_StoreDf = To_StoreDf.drop_duplicates()
    To_StoreDf = To_StoreDf.reset_index(drop=True)
    print(To_StoreDf.head())
    To_StoreDf = To_StoreDf.rename_axis(idT).reset_index()
    To_StoreDf = To_StoreDf.set_index(idT)

    maxId = 0
    if not sdf_Tbl.empty :
        
        print(sdf_Tbl.head())
        df_Tbl = sdf_Tbl[Tcolumns]
        # compare the two dfs to keep unique lines only from the file to insert 
        To_StoreDf = pd.merge(To_StoreDf, df_Tbl, on = Tcolumns, how='outer', indicator=True)
        To_StoreDf = To_StoreDf.query("_merge == 'left_only'")
        To_StoreDf = To_StoreDf.drop('_merge', axis=1)
        To_StoreDf = To_StoreDf.reset_index(drop=True)

        print(To_StoreDf.head())

        To_StoreDf = To_StoreDf.rename_axis(idT).reset_index()
        To_StoreDf = To_StoreDf.set_index(idT)
        
        maxId = sdf_Tbl[idT].max() +1 
        print(maxId)
        if not pd.isna(maxId) :
            #Modify the id  
            To_StoreDf.index = To_StoreDf.index + maxId


    return To_StoreDf

# Storing data into Database function 
def DB_Store (df, engine, schema, tbl_name):

    df.to_sql(tbl_name, engine, schema = schema, if_exists ='append', index = True)

#returns the attributes of a table as written in the database 
def db_attributes (engine, schema, tbl_name) :

    select_tbl = "select column_name from information_schema.columns where table_schema = '{0}' and table_name = '{1}';".format(schema, tbl_name)
    result = engine.execute(select_tbl)
    attributes =[]
    for i in  result.fetchall() :
        att = list(i)
        attributes.append(att[0])
    return attributes

# Destination: Postgres
pwd ="admin123"
uid = "postgres"
server = "localhost"
database = "COLLECTOR"
port = 5432
engine = DB_Connect(uid, pwd, server, database, port)

# Source
# Fichier Incubation
filename = 'Incubation_description'
file = True
path = File_Path(engine, BASE_DIR, filename, file)
IncubationDf = file_to_df(path)



###############################        t_project_pro, processing       ###############################
#Done

tbl_name = "t_project_pro"
schema = "ctl"
projectCloumns = db_attributes(engine, schema, tbl_name)
projectId = projectCloumns[0]
projectCloumns = projectCloumns[1:]


ProjectDfColumns = ["Project_name","Data_responsible","Other_contributors","laboratory_name_and_city","Additional_data_available_?_(to_be_filled_in_at_the_end)","Project_year"]

df = dbTbl_to_df (engine, schema, tbl_name)
ProjectDf = NFT_Transorm(IncubationDf, projectCloumns, ProjectDfColumns, projectId, df)

if not ProjectDf.empty :

    # Save the data to destination as the intial load. On the first run we load all data.
    DB_Store (ProjectDf, engine, schema, tbl_name)

else :
    print("Pas de nouvelle données à insérer en base")





###############################        t_soil_soi, processing       ###############################
#Done

soilColumns = ["soi_type","soi_classification"]
tbl_name = "t_soil_soi"
schema = "ctl"
soilColumns = db_attributes(engine, schema, tbl_name)
soilId = soilColumns[0]
soilColumns = soilColumns[1:]

SoilDfColumns = ["Soil_type","Soil_type_classification"]


df = dbTbl_to_df (engine, schema, tbl_name)
SoilDf = NFT_Transorm(IncubationDf,soilColumns,SoilDfColumns,soilId, df)

if not SoilDf.empty :

    # Save the data to destination as the intial load. On the first run we load all data.
    print('inseertion db')
    DB_Store (SoilDf, engine, schema, tbl_name)

else :
    print("Pas de nouvelle données à insérer en base")




# ###############################        t_site_sit, processing       ###############################
#Done

siteColumns = ["sit_municipality","sit_postal_code","sit_gps_coordinates","sit_ecosystem","sit_reference"]
tbl_name = "t_site_sit"
schema = "ctl"
siteColumns = db_attributes(engine, schema, tbl_name)
siteId = siteColumns[0]
siteColumns = siteColumns[1:]

SiteDfCOlumns = ["Soil_sampling_site_municipality","Soil_sampling_site_postal_code","Soil_sampling_site_GPS_location","Soil_sampling_site_ecosystem_name","Soil_sampling_site_reference"]



df = dbTbl_to_df (engine, schema, tbl_name)
SiteDF = NFT_Transorm(IncubationDf,siteColumns,SiteDfCOlumns,siteId, df)

if not SiteDF.empty :

    # Save the data to destination as the intial load. On the first run we load all data.
    print('inseertion db')
    DB_Store (SiteDF, engine, schema, tbl_name)

else :
    print("Pas de nouvelle données à insérer en base")




# ###############################        t_publication_pub, processing       ###############################
#Done 

tbl_name = "t_publication_pub"
schema = "ctl"
pubColumns = db_attributes(engine, schema, tbl_name)


pubId = pubColumns[0]
pubColumns = pubColumns[1:]

PubDfColumns = ["Publication_(DOI_or_URL)"]


df = dbTbl_to_df (engine, schema, tbl_name)
PubDf = NFT_Transorm(IncubationDf,pubColumns,PubDfColumns,pubId,df)

if not PubDf.empty :

    # Save the data to destination as the intial load. On the first run we load all data.
    print('inseertion db')
    DB_Store (PubDf, engine, schema, tbl_name)

else :
    print("Pas de nouvelle données à insérer en base")



# ###############################        t_litter_lit, processing       ###############################
# #Done
schema = "ctl"
tbl_name = "t_litter_lit"

litColumns = db_attributes(engine, schema, tbl_name)
litId = litColumns[0]
litColumns = litColumns[1:]


LitDfColumns = ["Litter_land_use","Plant_litter","Litter_variety","Litter_latin_name","Litter_plant_part","Litter_origin_country","Litter_origin_geographic_region","Litter_growth_medium","Litter_growth_nutrient_solution_composition","Litter_growth_nutrient_solution_concentration"]

df = dbTbl_to_df (engine, schema, tbl_name)
LitDF = NFT_Transorm(IncubationDf,litColumns,LitDfColumns,litId,df)
print(LitDF.columns)
LitDF['lit_nutrient_solution_concentration'] = LitDF['lit_nutrient_solution_concentration'].replace('ND',np.nan)
LitDF['lit_nutrient_solution_concentration'] = LitDF['lit_nutrient_solution_concentration'].replace('None',np.nan)
if not LitDF.empty :

    # Save the data to destination as the intial load. On the first run we load all data.
    print('inseertion to db')
    DB_Store (LitDF, engine, schema, tbl_name)

else :
    print("Pas de nouvelle données à insérer en base")



# ###############################        rt_data_category_dac, processing       ###############################
#Done
# #This table is static and will never change
# #not mode refers to un premier remplissage of the database

if not mode : 
    filename = 'RD_Mineral_P'
    file = False
    dir_source = File_Path(engine, BASE_DIR, filename, file)
    tbl_name = "rt_data_category_dac"
    schema = "ctl"
    categoryId = 'dac_id'

    files = os.listdir(dir_source)
    category = []
    for filename in files:
        cat = re.search(r'(?<=_)\w+', filename)
        cat = cat.group(0)
        category.append(cat)

    CategoryDf = pd.DataFrame(category, columns=["dac_label"])
    CategoryDf = CategoryDf.rename_axis(categoryId).reset_index()
    CategoryDf = CategoryDf.set_index(categoryId)
    DB_Store (CategoryDf, engine, schema, tbl_name)


# ###############################        t_input_inp, processing       ###############################

schema = "ctl"
tbl_name = "t_input_inp"

inputColumns = db_attributes(engine, schema, tbl_name)
inputId = inputColumns[0]
inputColumns = inputColumns[1:]

InputDfColumns = ["NH4+_added_by_solution","NO3-_added_by_solution","Mineral_N_added_by_solution","HPO42-_added_by_solution","H2PO4-_added_by_solution","K2SO4_added_by_solution","15NO3_added_by_solution"]


df = dbTbl_to_df (engine, schema, tbl_name)
InputDf = NFT_Transorm(IncubationDf,inputColumns,InputDfColumns,inputId,df)

if not InputDf.empty :

    # Save the data to destination as the intial load. On the first run we load all data.
    print('inseertion db')
    DB_Store (InputDf, engine, schema, tbl_name)

else :
    print("Pas de nouvelle données à insérer en base")




# ###############################        t_litter_sample_input_lsi, processing       ###############################
schema = "ctl"
tbl_name = "t_litter_sample_input_lsi"

lsiColumns = db_attributes(engine, schema, tbl_name)
lsiId = lsiColumns[0]
lsiColumns = lsiColumns[1:]


LsiDfColumns = ["Litter_organic_C_added","Litter_13C_added","Litter_organic_N_added","Litter_total_N_added","Litter_15N_added","Litter_organic_P_added","Litter_organic_S_added",
"Dry_litter_mass_added_(mg/kg_dry_soil)","Dry_litter_mass_added_(mg/m2_dry_soil)"]


df = dbTbl_to_df (engine, schema, tbl_name)
LsiDf = NFT_Transorm(IncubationDf,lsiColumns,LsiDfColumns,lsiId,df)

if not LsiDf.empty :

    # Save the data to destination as the intial load. On the first run we load all data.
    print('inseertion db')
    DB_Store (LsiDf, engine, schema, tbl_name)

else :
    print("Pas de nouvelle données à insérer en base")



# ###############################        t_incubation_conditions_inc, processing       ###############################

schema = "ctl"
tbl_name = "t_incubation_conditions_inc"

incColumns = db_attributes(engine, schema, tbl_name)
incId = incColumns[0]
incColumns = incColumns[1:]


IncColumnsDf =["Starting_date_of_C_incubation",
               "Number_of_C_incubation_repetitions",
               "Soil_dry_mass_of_C_incubation",
               "Dry_soil_thickness_of_C_incubation",
"Dry_soil_density_of_C_incubation",
"Volume_of_jar_used_for_C_incubation",
"Presence_of_a_vial_of_water_in_the_jar_for_C_incubation",
"Starting_date_of_N_incubation",
"Number_of_N_incubation_repetitions",
"Soil_dry_mass_of_N_incubation",
"Dry_soil_thickness_of_N_incubation",
"Dry_soil_density_of_N_incubation",
"Volume_of_jar_used_for_N_incubation",
"Presence_of_a_vial_of_water_in_the_jar_for_N_incubation",
"Starting_date_of_P_incubation",
"Number_of_P_incubation_repetitions",
"Soil_dry_mass_of_P_incubation",
"Dry_soil_thickness_of_P_incubation",
"Dry_soil_density_of_P_incubation",
"Volume_of_jar_used_for_P_incubation",
"Presence_of_a_vial_of_water_in_the_jar_for_P_incubation",
"Starting_date_of_S_incubation",
"Number_of_S_incubation_repetitions",
"Soil_dry_mass_of_S_incubation",
"Dry_soil_thickness_of_S_incubation",
"Dry_soil_density_of_S_incubation",
"Volume_of_jar_used_for_S_incubation",
"Presence_of_a_vial_of_water_in_the_jar_for_S_incubation"]


df = dbTbl_to_df (engine, schema, tbl_name)
IncDf = NFT_Transorm(IncubationDf,incColumns,IncColumnsDf,incId,df)
IncDf ['inc_water_vial_presence_c_incubation'] = IncDf['inc_water_vial_presence_c_incubation'].replace('Non','No')
IncDf ['inc_water_vial_presence_n_incubation'] = IncDf['inc_water_vial_presence_n_incubation'].replace('Oui','Yes')
IncDf ['inc_starting_date_n_incubation'] = IncDf['inc_starting_date_n_incubation'].replace('010/02/2000','01/02/2000')
IncDf ['inc_starting_date_n_incubation'] = IncDf['inc_starting_date_n_incubation'].replace('010/02/2001','01/02/2001')
IncDf ['inc_starting_date_n_incubation'] = IncDf['inc_starting_date_n_incubation'].replace('010/02/2002','01/02/2002')
IncDf ['inc_starting_date_n_incubation'] = IncDf['inc_starting_date_n_incubation'].replace('010/02/2003','01/02/2003')
IncDf ['inc_starting_date_n_incubation'] = IncDf['inc_starting_date_n_incubation'].replace('010/02/2004','01/02/2004')
IncDf ['inc_starting_date_n_incubation'] = IncDf['inc_starting_date_n_incubation'].replace('010/02/2005','01/02/2005')
IncDf ['inc_starting_date_n_incubation'] = IncDf['inc_starting_date_n_incubation'].replace('010/02/2006','01/02/2006')
IncDf ['inc_starting_date_n_incubation'] = IncDf['inc_starting_date_n_incubation'].replace('010/02/2007','01/02/2007')
IncDf ['inc_starting_date_n_incubation'] = IncDf['inc_starting_date_n_incubation'].replace('010/02/2008','01/02/2008')
IncDf ['inc_starting_date_n_incubation'] = IncDf['inc_starting_date_n_incubation'].replace('33679',None)
IncDf ['inc_starting_date_c_incubation'] = IncDf['inc_starting_date_c_incubation'].replace('33679',None)



if not IncDf.empty :

    # Save the data to destination as the intial load. On the first run we load all data.
    print('inseertion db')
    DB_Store (IncDf, engine, schema, tbl_name)

else :
    print("Pas de nouvelle données à insérer en base")




###################################           t_litter_sample_lis        ###################################
                                            ## FOREIGN KEY TABLES ##
schema = "ctl"
tbl_name = "t_litter_sample_lis"

lisColumns = db_attributes(engine, schema, tbl_name)
lisId = lisColumns[0]
lisColumns = lisColumns[1:]


LisColumnsDf =["Litter_collection_date", "Litter_collection_phenological_stage", "Litter_drying_temperature",
"Litter_drying_duration","Litter_pre-treatment","Litter_15N_initial_isotopic_excess","Litter_13C_initial_isotopic_excess",
"Litter_34S_initial_isotopic_excess","Litter_location_in_the_soil","Litter_preparation","Litter_particle_size_:_length", "Litter_particle_size_:_thickness"]

#1 

RecupColumns = LisColumnsDf + LitDfColumns

joinDF = IncubationDf [RecupColumns]
joinDF.columns = lisColumns[:-1] + litColumns
joinDF = joinDF.replace(np.nan , None)
joinDF ['lit_nutrient_solution_concentration'] = joinDF['lit_nutrient_solution_concentration'].replace('ND',None)
joinDF ['lit_nutrient_solution_concentration'] = joinDF['lit_nutrient_solution_concentration'].replace('nan',None)



#2
ref_tbl = dbTbl_to_df (engine, schema, "t_litter_lit")


#3
ref_tbl = ref_tbl.astype(str)
ref_tbl['new'] = ref_tbl[litColumns].apply(''.join, axis=1)
mapping_dict = dict(zip (ref_tbl['new'],ref_tbl[litId]))

joinDF = joinDF.astype(str)
joinDF ['new'] = joinDF[litColumns].apply(''.join, axis=1)

#4
#Mapping
joinDF[lisColumns[-1]] = joinDF["new"].map(mapping_dict) 
joinDF[lisColumns[-1]] = joinDF[lisColumns[-1]].astype(int)

#5
lisDf = joinDF[lisColumns]
print(lisDf.head())


